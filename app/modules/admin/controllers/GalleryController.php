<?php

namespace app\modules\admin\controllers;

use app\components\General;
use app\modules\admin\models\ImageUploader;
use Yii;
use app\modules\admin\models\ImagesUploader;
use app\models\Gallery;
use app\models\GallerySearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends CustomController
{

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $sort = ['id' => SORT_DESC];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sort);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $picNameArr = $errorsPic = [];

        $model = new Gallery();
        $imageModel = new ImagesUploader();
        $imageModel->scenario = "create";

        $imageModel->imageFiles = UploadedFile::getInstances($imageModel, 'imageFiles');
        if($imageModel->imageFiles != null)
        {
            $picNameArr = $imageModel->uploadMultiImage('gallery');
        }

        if ($model->load(Yii::$app->request->post()))
        {
            $model->file_name   = ($picNameArr != null) ? $picNameArr[0] : null;
            $model->images_file = ($picNameArr != null) ? json_encode($picNameArr) : null;
            $model->create_at   = time();
            if($model->save())
            {
                Yii::$app->session->setFlash('alert', ['success', General::showSummaryErrors($errorsPic) . 'عملیات مورد نظر با موفقیت انجام  شد.']);
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else
            {
                Yii::$app->session->setFlash('alert', ['success', General::showSummaryError($model->errors)]);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
                'imageModel' => $imageModel
            ]);
        }
    }
    public function actionMdelete($id,$name){

        $model = $this->findModel($id);
        $fileName=json_decode($model->images_file);
        $nameFile = array();
        if(count($fileName) > 1 ){
        foreach ($fileName as $value){
            if($value != $name){
                array_push($nameFile,$value);
            }
        }

        $model->images_file = json_encode($nameFile);
            $model->file_name = $nameFile[0];
            if($model->save()){

        }



        @unlink(Yii::getAlias('@storage-root').'/image/front/gallery/'. $name);
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    public function actionDeletePic()
    {
        $data = [];
        $id   = Yii::$app->request->post('id');
        $name = Yii::$app->request->post('name');

        $model = $this->findModel($id);

        if($model != null)
        {
            $picNames = json_decode($model->images_file, true);

            if (($key = array_search($name, $picNames)) !== false) {
                @unlink(Yii::getAlias('@storage-root') . '/image/front/gallery/' . $picNames[$key]);
                unset($picNames[$key]);
            }

            $model->images_file = json_encode(array_values($picNames));

            if($model->save())
            {
                $data = ['result' => true, 'message' => 'عملیات حذف با موفقیت انجام پذیرفت.'];
            }
            else
            {
                $data = ['result' => false, 'message' => json_encode($model->errors)];
            }

        }
        else
        {
            $data = ['result' => false, 'message' => 'چنین رکوردی وجود ندارد.'];
        }

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $data;


    }
    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $picNameArr = $errorsPic = [];

        $model = $this->findModel($id);
        $imageModel = new ImagesUploader();
        $imageModel->scenario = "update";

        $imageModel->imageFiles = UploadedFile::getInstances($imageModel, 'imageFiles');

        if($imageModel->imageFiles != null)
        {
            $picNameArr = $imageModel->uploadMultiImage('gallery');

            if($model->images_file != null)
            {
                /* if image_file database is not null we going to merge 2 arrays */
                $savedPicName = json_decode($model->images_file, true);
                $picNameArr   = array_merge($picNameArr, $savedPicName);
            }
            else
            {
                /* if image_file database equal to null after uploading we get the first pic of array  */
                $model->file_name = $picNameArr[0];
            }
        }

        if ($model->load(Yii::$app->request->post())) {

            if($model->file_name == null)
            {
                $model->file_name = ($picNameArr != null ? $picNameArr[0] : $model->images_file[0]);
            }

            if($picNameArr != null)
            {
                $model->images_file = json_encode($picNameArr);
            }

            if($model->save())
            {
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
                return $this->redirect(['view', 'id' => $model->id]);
            }

        } else {
            return $this->render('update', [
                'model' => $model,
                'imageModel' => $imageModel,
            ]);
        }
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->images_file != null)
        {
            foreach (json_decode($model->images_file) as $item)
            {
                @unlink(Yii::getAlias('@storage-root') . '/image/front/gallery/' . $item);
            }
        }

        $model->delete();
        Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
