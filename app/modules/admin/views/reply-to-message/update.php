<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReplyToMessage */

$this->title = 'Update Reply To Message: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Reply To Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reply-to-message-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
