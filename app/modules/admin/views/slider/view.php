<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'اسلایدر', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
        <div class="slider-view">

            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('حذف', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',

                    [
                        'attribute' => 'file_name',
                        'format'=>'html',
                        'value' => function ($data) {

                            return Html::img(Yii::getAlias('@storage-url').'/image/front/slider/' . $data->file_name,['width' => '150px']);
                        },
                    ],
                    [
                        'attribute' => 'visible',
                        'format' => 'raw',
                        'value' => $model->statusArr[$model->visible]
                    ],
                ],
            ]) ?>

        </div>

    </div>
</div>

