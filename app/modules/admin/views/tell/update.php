<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tell */

$this->title = 'ویرایش اطلاعات تماس';
$this->params['breadcrumbs'][] = ['label' => 'Tells', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ویرایش';
?>
<div class="tell-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
