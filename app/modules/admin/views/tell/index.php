<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TellSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'اطلاعات تماس';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
<div class="news-index">


    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'id',
            'telegram',
            'instagram',
            'facebook',
            'tell',
            //'address',

            [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update}',

                    ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
</div>

