<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tell */

$this->title = 'ایجاد تماس با ما';
$this->params['breadcrumbs'][] = ['label' => 'Tells', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
