<?php
namespace app\modules\admin\models;


use Yii;
use yii\web\UploadedFile;

class ImagesUploader extends \yii\base\Model
{

    /**
     * @var $imageFiles UploadedFile[]
     */
    public $imageFiles;
    public function rules()
    {
        return [
            [['imageFiles'], 'file','extensions' => 'jpeg, jpg, png', 'checkExtensionByMimeType' => true ,'skipOnEmpty' => false, 'maxFiles' => 5, 'on' => 'create'],
            [['imageFiles'], 'file','extensions' => 'jpeg, jpg, png', 'checkExtensionByMimeType' => true ,'skipOnEmpty' => true, 'maxFiles' => 5, 'on' => 'update'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'عکس',
        ];
    }



    public function uploadMultiImage($path)
    {
        $picNameArr = $errorsPic = [];
        $gd = Yii::$app->gd;
        foreach ($this->imageFiles as $item)
        {
            $gd->load($item->tempName);
            $gd->resizeToWidth(700);
            $picName =  Yii::$app->security->generateRandomString(5) . time() . '.' . $item->extension;
            $picNameArr[] = $picName;

            if(!is_dir(Yii::getAlias('@storage-root') . '/image/front/' . $path . '/')) mkdir(Yii::getAlias('@storage-root') . '/image/front/' . $path . '/', 0777, true);

            $gd->save(Yii::getAlias('@storage-root') . '/image/front/' . $path . '/' . $picName);
        }

        return $picNameArr;
    }
}