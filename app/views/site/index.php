<?php


use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $sliderModel  */
/* @var $serviceModel  */
/* @var $aboutModel  */
/* @var $galleryModel  */
/* @var $newsModel  */
$this->title ='شرکت حکاکی بصیر';
$this->registerMetaTag([
    'name' => 'description',
    'content' => '',
]);
?>
<div class="row">
    <div class="site-showcase">
        <div class="slider-mask overlay-transparent"></div>
        <!-- Start Hero Slider -->
        <div class="hero-slider flexslider clearfix" data-autoplay="yes" data-pagination="no" data-arrows="yes" data-style="fade" data-pause="yes">
            <ul class="slides">
                <?php $i=0;foreach ($sliderModel as $slider):?>
                    <li class=" parallax" style="background-image:url(<?= Yii::getAlias('@storage-url').'/image/front/slider/'.$slider->file_name;?>);">

                    </li>
                <?php endforeach;?>

            </ul>
        </div>
        <!-- End Hero Slider -->
        <!-- Site Search Module -->
        <!--		<div class="site-search-module">
                    <div class="container">
                        <div class="site-search-module-inside">
                            <form action="#">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select name="propery type" class="form-control input-lg selectpicker">
                                                <option selected disabled>نوع</option>
                                                <option>ویلا</option>
                                                <option>خانه خانوادگی</option>
                                                <option>خانه مجردی</option>
                                                <option>کلبه</option>
                                                <option>آپارتمان</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select name="propery contract type" class="form-control input-lg selectpicker">
                                                <option selected disabled>قرارداد</option>
                                                <option>اجاره</option>
                                                <option>خرید</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select name="propery location" class="form-control input-lg selectpicker">
                                                <option selected disabled>شهر</option>
                                                <option>تبریز</option>
                                                <option>تهران</option>
                                                <option>شیراز</option>
                                                <option>اصفهان</option>
                                                <option>ارومیه</option>
                                                <option>مشهد</option>
                                                <option>کرمان</option>
                                                <option>همدان</option>
                                                <option>سنندج</option>
                                                <option>رشت</option>
                                                <option>ساری</option>
                                                <option>بجنورد</option>
                                                <option>بندر عباس</option>
                                                <option>اهواز</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2"> <button type="submit" class="btn btn-primary btn-block btn-lg"><i class="fa fa-search"></i> جستجو</button> </div>
                                        <div class="col-md-2"> <a href="#" id="ads-trigger" class="btn btn-default btn-block"><i class="fa fa-plus"></i> <span>پیشرفته</span></a> </div>
                                    </div>
                                    <div class="row hidden-xs hidden-sm">
                                        <div class="col-md-2">
                                            <label>اتاق خواب</label>
                                            <select name="beds" class="form-control input-lg selectpicker">
                                                <option>همه</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                                <option>7</option>
                                                <option>8</option>
                                                <option>9</option>
                                                <option>10</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>سرویس بهداشتی</label>
                                            <select name="beds" class="form-control input-lg selectpicker">
                                                <option>همه</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                                <option>7</option>
                                                <option>8</option>
                                                <option>9</option>
                                                <option>10</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>حداقل قیمت (تومان)</label>
                                            <input type="text" class="form-control input-lg" placeholder="همه">
                                        </div>
                                        <div class="col-md-2">
                                            <label>حداکثر قیمت (تومان)</label>
                                            <input type="text" class="form-control input-lg" placeholder="همه">
                                        </div>
                                        <div class="col-md-2">
                                            <label>حداقل متراژ</label>
                                            <input type="text" class="form-control input-lg" placeholder="همه">
                                        </div>
                                        <div class="col-md-2">
                                            <label>حداکثر متراژ</label>
                                            <input type="text" class="form-control input-lg" placeholder="همه">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>-->

</div>
</div>
<div class="main" role="main">
    <div id="content" class="content full">
                <div class="featured-blocks">
                    <div class="container">
                        <div class="row">
                            <?php $i=0;foreach ($serviceModel as $service):?>
                                <div class="col-xs-12 col-sm-4 featured-block">
                                    <img  class="img-thumbnail" src="<?= Yii::getAlias('@storage-url').'/image/front/service/'.$service->file_name;?>" alt="">

                                    <h3><?= Html::encode($service->title) ?></h3>
                                    <div class="min-desc"><?= mb_substr($service->description, 0, 40) . '  ...' ?></div>
                                </div>
                            <?php $i++;endforeach;?>
                        </div>
                    </div>
                </div>

        <div class="spacer-40"></div>
        <div id="featured-properties">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block-heading">
                            <h4><span class="heading-icon"><i class="fa fa-star"></i></span>پربازدید ترین ها</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="owl-carousel owl-alt-controls " data-autoplay="no" data-pagination="no" data-arrows="yes">
                            <?php $i=0;foreach ($galleryModel as $gallery):?>
                                <li class="item property-block">
                                    <a href="<?=  Url::to(['gallery/view', 'id' => $gallery->id])?>" class="property-featured-image">
                                        <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/gallery/'.$gallery->file_name;?>" alt="">
                                        <span class="images-count"><i class="fa fa-picture-o"></i><?= count(json_decode($gallery->images_file,true))?></span>
                                        <?php
                                            $modelCate = \app\models\Category::find()->where(['id' => $gallery->category_id])->one();
                                        ?>

                                    </a>
                                    <div class="property-info">
                                        <h4><a href="#"><?= Html::encode($modelCate->title) ?></a></h4>
                                        <span class="location"><?= Html::encode($modelCate->title) ?></span>
                                    </div>
                                    <div class="property-amenities clearfix">
                                        <span class="area"><strong><?=$gallery->num_view;?></strong>تعداد بازدید</span>

                                    </div>
                                </li>
                            <?php endforeach;?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="spacer-40"></div>
        <div class="container">
            <div class="row">
                <div class="property-columns" id="latest-properties">
                    <div class="col-md-12">
                        <div class="block-heading">
                            <h4><span class="heading-icon"><i class="fa fa-leaf"></i></span>نمونه کارها</h4>
                            <a href="<?=  Url::to(['gallery/index'])?>" class="btn btn-primary btn-sm pull-left">مشاهده  بیشتر <i class="fa fa-long-arrow-left"></i></a>
                        </div>
                    </div>
                    <ul>
                        <?php $i=0;foreach ($galleryModel as $gallery):?>
                            <li class="col-md-4 col-sm-6 type-rent">
                                <div class="property-block">
                                    <a href="<?=  Url::to(['gallery/view', 'id' => $gallery->id])?>" class="property-featured-image">
                                        <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/gallery/'.$gallery->file_name;?>" alt="">

                                        <span class="images-count"><i class="fa fa-picture-o"></i><?= count(json_decode($gallery->images_file,true))?></span>
                                        <?php
                                        $modelCate = \app\models\Category::find()->where(['id' => $gallery->category_id])->one();
                                        ;?>

                                    </a>
                                    <div class="property-info">
                                        <h4><a href="<?=  Url::to(['gallery/view', 'id' => $gallery->id])?>"><?=$gallery->title;?></a></h4>
                                        <div class="min-desc"><?=$gallery->description;?></div>

                                    </div>
                                    <div class="property-amenities clearfix">
                                        <span class="area"><strong><?=$gallery->num_view;?></strong>تعداد بازدید</span>

                                    </div>
                                </div>
                            </li>
                        <?php endforeach;?>

                    </ul>
                </div>
            </div>
        </div>



<section>
    <div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="block-heading">
                <h4><span class="heading-icon"><i class="fa fa-leaf"></i></span>اخبار جدید</h4>
                <a href="<?=  Url::to(['news/index'])?>" class="btn btn-primary btn-sm pull-left">مشاهده خبرهای های بیشتر <i class="fa fa-long-arrow-left"></i></a>
            </div>
            <div class="property-listing">
                <ul>
                    <?php $i=0;foreach ($newsModel as $news):?>
                        <li class="type-rent col-md-12">
                            <div class="col-md-4">
                                <a href="<?=  Url::to(['news/view', 'id' => $gallery->id])?>" class="property-featured-image">

                                    <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/news/'.$news->file_name;?>" alt="">
                                </a>
                            </div>
                            <div class="col-md-8">
                                <div class="property-info">

                                    <h3><a href="<?=  Url::to(['news/view', 'id' => $gallery->id])?>"><?= $news->title?></a></h3>
                                    <span class="location"><?= $news->title?></span>
                                    <div class="min-desc"> <?= $news->description?></div>
                                </div>
                                <div class="property-amenities clearfix">
                                    <span class="area"><strong><?=$gallery->num_view;?></strong>تعداد بازدید</span>
                                </div>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
        <!-- Start Sidebar -->
        <div class="sidebar right-sidebar col-md-3">

            <div class="widget-recent-posts widget">
                <h3 class="widgettitle">جدیدترین مطالب</h3>
                <ul>
                    <?php $i=0;foreach ($newsModel as $news):?>
                        <li class="clearfix">
                            <a href="<?=  Url::to(['news/view', 'id' => $news->id])?>" class="media-box post-image">
                                <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url') . '/image/front/news/' . $news->file_name ?>" alt="">
                            </a>
                            <div class="widget-blog-content">
                                <a href="<?=  Url::to(['news/view', 'id' => $gallery->id])?>"><?= Html::encode($news->title) ?></a>
                            </div>
                        </li>

                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
    </div>


</div>
</div>
