<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 10/3/2017
 * Time: 6:28 PM
 */

/*  @var $model*/
$this->title ='تماس با ما';
$this->registerMetaTag([
    'name' => 'description',
    'content' => ' '.$this->title,
]);

$this->params['breadcrumbs'][] = $this->title;


?>
<div class="container ">
    <?= \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <div class="news">
        <section>
            <div class="section page-title-main">
                <div class="row">

                    <div class=" col-sm-12 col-xs-12">
                        <h1> تماس با شرکت</h1>
                        <div class="center">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="section">
                <div class="row">
                    <div class="activity col-sm-12 col-xs-12">
                        <div class="col-sm-12 col-xs-12">
                            <?php if($model != null){ ?>
                            <label for="">آدرس :</label><p><?= $model->address ;?></p>
                            <label for="">تلفن ها:</label><p><?= $model->tell ;?></p>
                            <?php } ?>
                        </div>

                    </div>

                </div>
            </div>
        </section>
    </div>
</div>