<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\News;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);

$category = \app\models\Category::find()->all();
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/storage/img/site/favicon.ico" type="image/x-icon">
</head>
<body>

<body class="home">
    <?php $this->beginBody() ?>
        <div class="body">
            <div class="wrap main-content">
                <?php $aboutModel = \app\models\Tell::find()->orderBy(['id' => SORT_DESC])->one();?>
                <header class="site-header">
                    <div class="top-header hidden-xs">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-6">
                                    <ul class="horiz-nav pull-right">
                                        <li><a href="<?= $aboutModel->instagram != null ? Html::encode($aboutModel->instagram) : '' ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="<?= $aboutModel->facebook != null ? Html::encode($aboutModel->facebook) : '' ?>" target="_blank"><i class="fa fa-facebook"></i></a></i></a></li>
                                        <li><a href="<?= $aboutModel->telegram != null ? Html::encode($aboutModel->telegram) : '' ?>" target="_blank"><i class="fa fa-telegram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="middle-header">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-sm-8 col-xs-8">
                                    <h1 class="logo"> <a href=""> <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/site/logo.png';?>" alt=""></a> شرکت حکاکی لیزری بصیر</h1>
                                </div>
                                <div class="col-md-7 col-sm-4 col-xs-4">
                                    <div class="contact-info-blocks hidden-sm hidden-xs">
                                        <div>
                                            <?php $aboutModel = \app\models\Tell::find()->orderBy(['id' => SORT_DESC])->one();?>
                                            <i class="fa fa-phone"></i> تلفن تماس
                                            <span class="ltr_text"><?= Html::encode($aboutModel->tell)?></span>
                                        </div>
                                        <div>
                                            <i class="fa fa-envelope"></i> آدرس ایمیل
                                            <span>info@basir.com</span>
                                        </div>

                                    </div>
                                    <a href="#" class="visible-sm visible-xs menu-toggle"><i class="fa fa-bars"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-menu-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <nav class="navigation">
                                        <ul class="sf-menu">
                                            <li>
                                                <a href="<?=  Url::to(['/'])?>">خانه</a>
                                            </li>
                                            <li>
                                                <a href="<?=  Url::to(['/service' ])?>">خدمات</a>

                                            </li>
                                            <li>
                                                <a>نمونه کارها</a>
                                                <?php if($category != null): ?>
                                                    <ul class="dropdown" style="display: none;">
                                                        <?php
                                                            foreach ($category as $item => $value):
                                                        ?>
                                                            <li><a href="<?= Url::to(['/gallery/', 'id' => $value->id])?>"><?= $value->title?></a></li>
                                                        <?php endforeach;?>
                                                    </ul>
                                                <?php endif;?>

                                            </li>
                                            <li><a href="<?=  Url::to(['/news' ])?>">آخرین اخبار</a></li>
                                            <li><a href="<?=  Url::to(['/about-us' ])?>">درباره ما</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                        <?= $content ?>
                <footer class="site-footer">
                    <div class="container">
                        <div class="row">
                            <?php
                                $newsModel = News::find()->where(['visible' => 1])->orderBy(['id' => SORT_DESC])->limit(4)->all();
                                $serviceModel = \app\models\Service::find()->where(['visible' => 1])->orderBy(['id' => SORT_DESC])->limit(4)->all();
                                $galleryModel = \app\models\Gallery::find()->where(['visible' => 1])->orderBy(['id' => SORT_DESC])->limit(4)->all();
                            ?>
                            <div class="col-sm-4 footer-widget widget">
                                <h3 class="widgettitle">جدیدترین خبر ها</h3>
                                <ul>
                                    <?php foreach ($newsModel as $news):?>
                                    <li>
                                        <a href="<?=  Url::to(['news/view', 'id' => $news->id])?>"><?= Html::encode($news->title) ?></a>
                                    </li>
                                    <?php endforeach; ?>

                                </ul>
                            </div>
                            <div class="col-sm-4 footer-widget widget">
                                <h3 class="widgettitle">لیست خدمات</h3>
                                <ul>
                                    <?php foreach ($serviceModel as $serve):?>
                                        <li><a href="<?=  Url::to(['service/view', 'id' => $serve->id])?>"><?= Html::encode($serve->title) ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="col-sm-4 footer-widget widget">
                                <div class="contact-info-blocks hidden-sm hidden-xs">
                                    <div class="col-sm-12">

                                        <i class="fa fa-phone"></i> تلفن تماس
                                        <span class="ltr_text"><?= Html::encode($aboutModel->tell) ?></span>
                                    </div>
                                    <div class="col-sm-12">
                                        <i class="fa fa-envelope"></i> آدرس ایمیل
                                        <span>info@basir.com</span>
                                    </div>
                                    <div class="col-sm-12">
                                        <i class="fa fa-clock-o"></i> آدرس
                                        <span><?= Html::encode($aboutModel->address) ?></span>
                                    </div>
                                </div>
                                <a href="#" class="visible-sm visible-xs menu-toggle"><i class="fa fa-bars"></i></a>
                            </div>
                        </div>
                    </div>
                </footer>
                <footer class="site-footer-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="copyrights-col-left col-md-6 col-sm-6">
                                <p>ارائه شده در وب‌سایت <a href="">آرنگ گراف</a></p>
                            </div>
                            <div class="copyrights-col-right col-md-6 col-sm-6">
                                <div class="social-icons">
                                    <a href="<?= $aboutModel->facebook != null ? Html::encode($aboutModel->facebook) : '' ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="<?= $aboutModel->instagram != null ? Html::encode($aboutModel->instagram) : ''?>" target="_blank"><i class="fa fa-instagram"></i></a>
                                    <a href="<?= $aboutModel->telegram != null ? Html::encode($aboutModel->telegram)  : ''?>" target="_blank"><i class="fa fa-telegram"></i></a>

                                    <a href="#"><i class="fa fa-rss"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
