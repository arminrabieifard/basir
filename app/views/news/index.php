<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/*  @var $model*/
/*  @var $pages*/

$this->title = 'لیست خبرها';

//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="row">
                <div class="col-md-9">
                    <div class="block-heading">
                        <h4><span class="heading-icon"><i class="fa fa-th-list"></i></span>لیست اخبار</h4>

                    </div>
                    <div class="property-listing">
                        <ul>
                            <?php $i=0;foreach ($model as $news):?>
                            <li class="type-rent col-md-12">
                                <div class="col-md-4"> <a href="<?=  Url::to(['view', 'id' => $news->id])?>" class="property-featured-image">
                                        <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/news/'.$news->file_name;?>" alt="">
                                         </a> </div>
                                <div class="col-md-8">
                                    <div class="property-info">

                                        <h3><a href="<?=  Url::to(['view', 'id' => $news->id])?>"><?= $news->title?></a></h3>

                                        <p><?= $news->description?></p>
                                    </div>
                                    <div class="property-amenities clearfix"> <span class="area"><strong><?= $news->num_view?></strong>تعداد بازدید</span> </div>
                                </div>
                            </li>
                            <?php endforeach;?>

                        </ul>
                    </div>
                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
                <!-- Start Sidebar -->
                <div class="sidebar right-sidebar col-md-3">
                    <div class="widget sidebar-widget featured-properties-widget">
                        <h3 class="widgettitle">خبر های پر بازدید</h3>
                        <ul class="owl-carousel owl-alt-controls1 single-carousel" data-items-desktop="1" data-autoplay="no" data-pagination="no" data-arrows="yes">
                            <?php $i=0;foreach ($model2 as $news):?>
                            <li class="item property-block"> <a href="<?=  Url::to(['view', 'id' => $news->id])?>" class="property-featured-image">
                                    <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/news/'.$news->file_name;?>" alt="">
                                     </a>
                                <div class="property-info">
                                    <h4><a href="#"><?= $news->title?></a></h4>

                                </div>
                            </li>
                            <?php endforeach;?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



