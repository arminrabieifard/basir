<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/*  @var $model*/
/*  @var $pages*/

$this->title = 'لیست خبرها';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="block-heading">
                        <h4><span class="heading-icon"><i class="fa fa-th-large"></i></span>آخرین نمونه کارها</h4>

                    </div>
                    <div class="property-grid">
                        <ul class="grid-holder col-3">
                            <?php $i=0;foreach ($model as $gallery):?>
                            <li class="grid-item type-rent">
                                <div class="property-block"> <a href="<?=  Url::to(['view', 'id' => $gallery->id])?>" class="property-featured-image">
                                        <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/gallery/'.$gallery->file_name;?>" alt="">
                                        <span class="images-count"><i class="fa fa-picture-o"></i><?= count(json_decode($gallery->images_file, true))?></span></a>
                                    <div class="property-info">
                                        <h4><a href="<?=  Url::to(['view', 'id' => $gallery->id])?>"><?=$gallery->title;?></a></h4>
                                        <?php
                                        $modelCate = \app\models\Category::find()->where(['id' => $gallery->category_id])->one();
                                        ?>
                                       <!--
                                        <div class="price"><strong>زیر مجموعه:</strong><span><?/*= Html::encode($modelCate->title)*/?></span></div>
                                        -->
                                    </div>
                                    <div class="property-amenities clearfix"> <span class="area"><strong><?=$gallery->num_view;?></strong>تعداد بازدید</span>
                                         </div>
                                </div>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
