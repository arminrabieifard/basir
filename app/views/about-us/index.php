<?php

/*  @var $model*/
/*  @var $model2*/

if ($model == null){
   $title = 'درباره شرکت بصیر' ;
}
else{
    $title = $model->title;
}

$this->title = $title;

$this->registerMetaTag([
    'name' => 'description',
    'content' => ' '.$title,
]);


//$this->params['breadcrumbs'][] = $this->title;

?>
<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="page">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h3> <?php if($model != null){ echo $model->title; } ?></h3>
                        <?php if($model != null){ echo $model->description; } ?>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <h3>آخرین خدمات ما</h3>
                        <!-- Start Accordion -->
                        <div class="accordion" id="accordionArea">
                            <?php $i=0;foreach ($model2 as $model): ?>
                            <div class="accordion-group panel">
                                <div class="accordion-heading accordionize">
                                    <a class="accordion-toggle <?php if($i==0){echo 'active';} ?>" data-toggle="collapse" data-parent="#accordionArea" href="#<?= $model->id?>"> <?= $model->title?><i class="fa fa-angle-down"></i> </a> </div>
                                <div id="<?= $model->id?>" class="accordion-body <?php if($i==0){echo 'in';} ?> collapse">
                                    <div class="accordion-inner"><?= $model->description?></div>
                                </div>
                            </div>
                            <?php $i++; endforeach; ?>
                        </div>
                        <!-- End Accordion -->
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
