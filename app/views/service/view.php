<?php

use yii\helpers\Url;

/*  @var $model*/
$this->title =$model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => ' '.$model->title,
]);
$this->params['breadcrumbs'][] = ['label' => 'خدمات', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="row">
                <div class="col-md-9">
                    <div class="single-property">
                        <h2 class="page-title"><?= $model->title ;?> </h2>
                        <div class="price"><span><?= $model->num_view ;?></span><strong>تعداد بازدید</strong></div>

                        <div class="property-slider">
                            <div class="fotorama" data-nav="thumbs" data-autoplay="true" data-loop="true">



                                <img src="<?= Yii::getAlias('@storage-url') . '/image/front/service/' . $model->file_name; ?>">


                            </div>
                        </div>
                        <div class="tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"> <a data-toggle="tab" href="#description"> توضیحات </a> </li>

                            </ul>
                            <div class="tab-content">
                                <div id="description" class="tab-pane active">
                                    <?= $model->description ;?>
                                </div>

                            </div>
                        </div>

                    </div>
                    <!-- Start Related Properties -->

                </div>
                <!-- Start Sidebar -->
                <div class="sidebar right-sidebar col-md-3">

                    <div class="widget sidebar-widget featured-properties-widget">
                        <h3 class="widgettitle">اخرین نمونه کار ها</h3>
                        <ul class="owl-carousel owl-alt-controls1 single-carousel" data-items-desktop="1" data-autoplay="no" data-pagination="no" data-arrows="yes">
                            <?php $i=0;foreach ($model2 as $gallery):?>
                                <li class="item property-block">
                                    <a href="<?=  Url::to(['/gallery/view', 'id' => $gallery->id])?>" class="property-featured-image">
                                        <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/gallery/'.$gallery->file_name;?>" alt="">
                                    </a>
                                    <div class="property-info">
                                        <h4><a href="<?=  Url::to(['/gallery/view', 'id' => $gallery->id])?>"><?=$gallery->title;?></a></h4>

                                        <div class="price"><span><?=$gallery->num_view;?></span><strong>تعداد بازدید</strong></div>
                                    </div>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>