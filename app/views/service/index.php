<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
/*  @var $model*/
/*  @var $pages*/
/*  @var $cate*/
$this->title = 'لیست خدمات';
//$this->params['breadcrumbs'][] = $this->title;

?>

<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="block-heading">
                        <h4><span class="heading-icon"><i class="fa fa-th-large"></i></span>آخرین سرویس ها</h4>

                    </div>
                    <div class="property-grid">
                        <ul class="grid-holder col-3">
                            <?php $i=0;foreach ($model as $gallery):?>
                                <li class="grid-item type-rent">
                                    <div class="property-block">
                                        <a href="<?=  Url::to(['view', 'id' => $gallery->id])?>" class="property-featured-image">
                                            <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/service/'.$gallery->file_name;?>" alt="">
                                        </a>
                                        <div class="property-info">
                                            <h4><a href="<?=  Url::to(['view', 'id' => $gallery->id])?>"><?=$gallery->title;?></a></h4>


                                        </div>
                                        <div class="property-amenities clearfix"> <span class="area"><strong><?=$gallery->num_view;?></strong>تعداد بازدید</span>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
