<?php

use yii\db\Migration;

/**
 * Handles the creation of table `service`.
 */
class m180205_145656_create_service_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable('service', [
                'id' => $this->primaryKey(),
                 'category_id'=> $this->integer(11)->unsigned()->notNull()->comment('مربوط به دسته'),
                'title'=> $this->string(100)->notNull()->comment('عنوان'),
                'description'=> $this->text()->notNull()->comment('توضیحات'),
                'file_name'=> $this->string(50)->null()->defaultValue(null)->comment('نام فایل'),
                'num_view'=> $this->integer(11)->null()->defaultValue(0)->comment('تعداد بازدید'),
                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );
        $this->createIndex('idx-service-category_id','{{%service}}',['category_id'],false);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropIndex('idx-service-category_id', '{{%service}}');
        $this->dropTable('service');

    }
}
