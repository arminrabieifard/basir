<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085759_user extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%user}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'full_name'=> $this->string(50)->null()->defaultValue(null)->comment('نام و نام خانوادگی'),
                'user_name'=> $this->string(30)->notNull()->comment('نام کاربری'),
                'password'=> $this->string(250)->notNull()->comment('رمز عبور'),
                'auth_key'=> $this->string(32)->null()->defaultValue(null),
                'password_reset_token'=> $this->string(50)->null()->defaultValue(null),
                'email'=> $this->string(250)->null()->defaultValue(null)->comment('ایمیل'),
                'email_active'=> $this->smallInteger(1)->null()->defaultValue(0)->comment('وضعیت ایمیل'),
                'email_activation_token'=> $this->string(50)->null()->defaultValue(null),
                'active'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('وضعیت'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );
        $this->createIndex('user_name','{{%user}}',['user_name'],true);

    }

    public function safeDown()
    {
        $this->dropIndex('user_name', '{{%user}}');
        $this->dropTable('{{%user}}');
    }
}
