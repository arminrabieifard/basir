<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tell`.
 */
class m180205_185429_create_tell_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%tell}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'telegram'=> $this->string(255)->null()->defaultValue(null)->comment('آدرس تلگرام'),
                'instagram'=> $this->string(255)->null()->defaultValue(null)->comment('آدرس اینستاگرام'),
                'facebook'=> $this->string(255)->null()->defaultValue(null)->comment('آدرس فیسبوک'),
                'tell'=> $this->string(255)->null()->defaultValue(null)->comment('تلفن ها'),
                'address'=> $this->string(255)->null()->defaultValue(null)->comment('آدرس'),

            ],$tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tell');
    }
}
