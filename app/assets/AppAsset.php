<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/storage';
    public $baseUrl = '@web/storage';
    public $css = [
        'css/general/bootstrap-rtl.min.css',
        'css/general/font-awesome.min.css',
        'css/front/fotorama.css',
        'css/front/style.css',
        'css/front/prettyPhoto.css',
        'css/front/owl.carousel.css',
        'css/front/owl.theme.css',
        'css/front/color1.css',
        'css/front/style-switcher.css',
    ];
    public $js = [
        'js/general/bootstrap.min.js',
        'js/general/modernizr.js',
        'js/general/prettyphoto.js',
        'js/general/owl.carousel.min.js',
        'js/general/jquery.flexslider.js',
        'js/general/fotorama.js',
        'js/general/site.js',
        'js/general/helper-plugins.js',
        'js/general/waypoints.js',
        'js/general/init.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
