<?php

namespace app\controllers;

use app\models\Service;
use Yii;
use yii\web\Controller;
use app\models\AboutUs;

class AboutUsController extends Controller
{

    public function actionIndex()
    {
        $model = AboutUs::findOne(1);
        $model2 = Service::find()->where(['visible' => 1])->limit(4)->orderBy(['id' => SORT_DESC])->all();

        return $this->render('index', [
            'model' => $model,
            'model2' => $model2,
        ]);
    }
}