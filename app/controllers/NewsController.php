<?php
/**
 * Created by PhpStorm.
 * User: lvl
 * Date: 11/17/2017
 * Time: 8:18 AM
 */

namespace app\controllers;

use app\models\News;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

class NewsController extends Controller
{

    public function actionIndex()
    {
        $model = News::find()->where(['visible' => 1]);
        if(!empty($model)) {
            $pages = new Pagination([
                'totalCount' => $model->count(),
                'defaultPageSize' => 20,
            ]);


            $model = $model->offset($pages->offset)->limit($pages->limit)->orderBy(['id' => SORT_DESC])->all();
            $model2 = News::find()->where(['visible' => 1])->limit(4)->orderBy(['num_view' => SORT_ASC])->all();

            return $this->render('index', [
                'model' => $model,
                'pages' => $pages,
                'model2' => $model2

            ]);
        }
        else
        {
            return $this->render(['index']);
        }
    }
    public function actionView($id)
    {
        $model = News::find()->where(['id' => $id])->one();
        $model->num_view =$model->num_view * 1  + 1  ;
        $model->save();
        $model2 = News::find()->where(['visible' => 1])->limit(4)->orderBy(['id' => SORT_DESC])->all();;
        return $this->render('view', [
            'model' => $model,
            'model2' => $model2,

        ]);
    }
}