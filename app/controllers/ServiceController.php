<?php
/**
 * Created by PhpStorm.
 * User: lvl
 * Date: 11/17/2017
 * Time: 8:18 AM
 */

namespace app\controllers;

use app\models\Gallery;
use app\models\News;
use app\models\Service;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

class ServiceController extends Controller
{

    public function actionIndex()
    {
        $model = Service::find()->where(['visible' => 1]);

        $pages = new Pagination([
            'totalCount' => $model->count(),
            'defaultPageSize' => 20,
        ]);

        $model = $model->offset($pages->offset)->limit($pages->limit)->orderBy(['id' => SORT_DESC])->all();



        return $this->render('index', [
            'model' => $model,
            'pages' => $pages,
        ]);
    }

    public function actionView($id)
    {
        $model = Service::find()->where(['id' => $id])->one();
        $model->num_view =$model->num_view * 1  + 1  ;
        $model->save();
        $model2 = Gallery::find()->where(['visible' => 1])->limit(4)->orderBy(['id' => SORT_DESC])->all();
        return $this->render('view', [
            'model' => $model,
            'model2' => $model2,

        ]);
    }
}