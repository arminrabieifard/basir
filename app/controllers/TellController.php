<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 2/6/2018
 * Time: 10:39 PM
 */

namespace app\controllers;


use app\models\Tell;
use yii\web\Controller;

class TellController extends Controller
{
    public function actionIndex()
    {
        $model = Tell::findOne(1);

        return $this->render('index', [
            'model' => $model,
        ]);
    }

}