<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_Us`.
 */
class m171030_161208_create_about_us_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('about_us', [
            'id'          => $this->primaryKey(11)->unsigned(),
            'title'       => $this->string(255)->comment('عنوان'),
            'description' => $this->text()->comment('متن'),
            'create_at'   => $this->integer()->notNull()->defaultValue(0)->comment('زمان افزودن'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('about_us');
    }
}
