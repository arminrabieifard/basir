<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%service}}".
 *
 * @property int $id

 * @property string $title عنوان
 * @property string $description توضیحات
 * @property string $file_name نام فایل
 * @property int $num_view تعداد بازدید
 * @property int $visible نمایش داده شود؟
 * @property int $create_at زمان افزودن
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'title', 'description'], 'required'],
            [[ 'num_view', 'visible', 'create_at'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 100],
            [['file_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'عنوان',
            'description' => 'توضیحات',
            'file_name' => 'فایل',
            'num_view' => 'تعداد بازدید',
            'visible' => 'نمایش داده شود؟',
            'create_at' => 'زمان افزودن',
        ];
    }
}
