<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tell}}".
 *
 * @property string $id
 * @property string $telegram آدرس تلگرام
 * @property string $instagram آدرس اینستاگرام
 * @property string $facebook آدرس فیسبوک
 * @property string $tell تلفن ها
 * @property string $address آدرس
 */
class Tell extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tell}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['telegram', 'instagram', 'facebook', 'tell', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telegram' => 'آدرس تلگرام',
            'instagram' => 'آدرس اینستاگرام',
            'facebook' => 'ادرس فیسبوک',
            'tell' => 'تلفن ها',
            'address' => 'آدرس',
        ];
    }
}
